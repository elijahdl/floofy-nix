{ config, pkgs, lib, stylix, ... }: {
  home.username = "elijahdl";
  home.homeDirectory = "/home/elijahdl";
  home.stateVersion = "23.11"; # To figure this out you can comment out the line and see what version it expected.
  programs.home-manager.enable = true;

  imports = [
    ./desktop/arch-linux.nix
    ./desktop/shell.nix
  ];


  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [
      "electron-24.8.6"
      "electron-25.9.0"
    ];
  };

  stylix = {
    enable = true;
    image = ./style/okami-wallpaper.jpg;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/darkviolet.yaml";
  };

  home = {
    packages = with pkgs; [
      autojump
      comma
      cowsay
      fsearch
      glow # markdown previewer in terminal
      graphviz
      hugo # static site generator
      ipcalc # it is a calculator for the IPv4/v6 addresses
      livecaptions
      magic-wormhole-rs
      neofetch
      nix-index
      nixpkgs-fmt
      pandoc
      pipx
      scrcpy
      tmux
      tut
      warp
      wol
    ];

    shellAliases = {
      nfs = "nix run . -- switch --flake .";
      nfb = "nix run . -- build --flake .";

      yt-dl = "yt-dlp --restrict-filename -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'";
    };
  };

  programs = {

    wezterm = {
      enable = true;
      extraConfig = ''
        				${lib.fileContents ./programs/wezterm/wezterm.lua}
        				'';
    };
  };
}
