# Edit thVjjjis configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

with config.lib.stylix.colors;

{
  imports = [ ./games.nix ];

  # Make swaylock work
  security.pam.services.swaylock = {
    text = ''
      auth include login
    '';
  };

  security.rtkit.enable = true;

  hardware.pulseaudio.enable = false;

  environment.systemPackages = with pkgs; [
    brightnessctl
    fzf # A command-line fuzzy finder
    kitty
    killall
    swaylock
    wev
    wofi
  ];

  programs = {
    hyprland = {
      enable = true;
    };
  };

  services = {
    libinput.enable = true;
    pipewire = {
      enable = true;
      alsa =
        {
          enable = true;
          support32Bit = true;
        };
      pulse.enable = true;
      audio = {
        enable = true;
      };
      jack = {
        enable = true;
      };
    };
    xserver = {
      enable = true;
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
    };
  };

  stylix = lib.mkForce {
    image = ../style/wallpaper.png;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/darkviolet.yaml";
    targets = {
      plymouth.enable = false;
    };
  };
}
