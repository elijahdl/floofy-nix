{ config, pkgs, lib, ... }:

with config.lib.stylix.colors;

let
  brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
  grimblast = "${pkgs.grimblast}/bin/grimblast";
  hyprctl = lib.getExe' config.wayland.windowManager.hyprland.package "hyprctl";
  li-gestures = "${pkgs.libinput-gestures}/bin/libinput-gestures";
  playerctl = "${pkgs.playerctl}/bin/playerctl";
  swaybg = "${pkgs.swaybg}/bin/swaybg";
  swaylock = lib.getExe' config.programs.swaylock.package "swaylock";
  waybar = lib.getExe' config.programs.waybar.package "waybar";

  screenshot-fullscreen-editor = pkgs.writeShellApplication {
    name = "screenshot-fullscreen-editor.sh";
    text = ''
      export GRIMBLAST_EDITOR=ksnip
      ${grimblast} --cursor edit
    '';
  };
  screenshot-area-editor = pkgs.writeShellApplication {
    name = "screenshot-area-editor.sh";
    text = ''
      export GRIMBLAST_EDITOR=ksnip
      ${grimblast} --cursor edit area
    '';
  };
in
{
  home = {
    packages = with pkgs; [
      cliphist
      ksnip
      libinput-gestures
      networkmanager_dmenu
      wl-clipboard
    ];
    file = {
      ".config/libinput-gestures.conf".text = ''
        gesture swipe up        4 hyprctl dispatch togglespecialworkspace ferdium
        gesture swipe down        4 hyprctl dispatch togglespecialworkspace social
        gesture swipe left        4 hyprctl dispatch togglespecialworkspace music
      '';
      ".config/networkmanager-dmenu/config.ini".text = ''
        [dmenu]
        dmenu_command = ${pkgs.wofi}/bin/wofi --show dmenu
        # # Note that dmenu_command can contain arguments as well like:
        # # `dmenu_command = rofi -dmenu -i -theme nmdm`
        # # `dmenu_command = rofi -dmenu -width 30 -i`
        # # `dmenu_command = dmenu -i -l 25 -b -nb #909090 -nf #303030`
        # # `dmenu_command = fuzzel --dmenu`
        rofi_highlight = true
        # compact = <True or False> # (Default: False). Remove extra spacing from display
        # pinentry = <Pinentry command>  # (Default: None) e.g. `pinentry-gtk`
        # wifi_chars = <string of 4 unicode characters representing 1-4 bars strength>
        # wifi_chars = ▂▄▆█
        # wifi_icons = <characters representing signal strength as an icon>
        wifi_icons = 󰤯󰤟󰤢󰤥󰤨
        # format = <Python style format string for the access point entries>
        # format = {name}  {sec}  {bars}
        # # Available variables are:
        # #  * {name} - Access point name
        # #  * {sec} - Security type
        # #  * {signal} - Signal strength on a scale of 0-100
        # #  * {bars} - Bar-based display of signal strength (see wifi_chars)
        # #  * {icon} - Icon-based display of signal strength (see wifi_icons)
        # #  * {max_len_name} and {max_len_sec} are the maximum lengths of {name} / {sec}
        # #    respectively and may be useful for formatting.
        # list_saved = <True or False> # (Default: False) list saved connections

        [dmenu_passphrase]
        # # Uses the -password flag for Rofi, -x for bemenu. For dmenu, sets -nb and
        # # -nf to the same color or uses -P if the dmenu password patch is applied
        # # https://tools.suckless.org/dmenu/patches/password/
        # obscure = True
        # obscure_color = #222222

        [pinentry]
        # description = <Pinentry description> (Default: Get network password)
        # prompt = <Pinentry prompt> (Default: Password:)

        [editor]
        terminal = kitty
        # gui_if_available = <True or False> (Default: True)

        [nmdm]
        # rescan_delay = <seconds>  # (seconds to wait after a wifi rescan before redisplaying the results)
      '';
    };
  };

  programs = {
    swaylock = {
      enable = true;
      package = pkgs.swaylock-effects;
      settings = {
        daemonize = true;
        effect-blur = "50x10";
      };
    };
  };

  services = {
    dunst.enable = true;
    swayidle = {
      enable = true;
      timeouts = [
        {
          timeout = 10;
          command = "if pgrep -x swaylock; then ${hyprctl} dispatch dpms off; fi";
          resumeCommand = "${hyprctl} dispatch dpms on";
        }
        { timeout = 300; command = "${swaylock} -f"; }
        {
          timeout = 315;
          command = "${hyprctl} dispatch dpms off";
          resumeCommand = "${hyprctl} dispatch dpms on";
        }
      ];
    };
  };

  wayland.windowManager.hyprland =
    let
      rgb = color: "rgb(${color})";
      rgba = color: alpha: "rgba(${color}${alpha})";
    in
    {
      enable = true;

      settings = {
        # Source a file (multi-file configs)
        # source = ~/.config/hypr/myColors.conf

        debug = {
          disable_scale_checks = true; # needed to set display scale to 1.5
        };

        exec-once = [
          "${swaybg} -i ${../style/wallpaper.png}"
          "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"

          # Clipboard manager
          "wl-paste --type text --watch cliphist store"
          "wl-paste --type image --watch cliphist store"

          # Desktop applicatns
          "zen"
          "[noanim] telegram-desktop"
          "[noanim] webcord"
          "[noanim] ferdium"
          "[workspace 7, noanim] obsidian"
        ];

        exec = [
          "pkill .waybar-wrapped ; ${waybar} &"
          "pkill .libinput-gestu ; ${li-gestures} &"
        ];

        # See https://wiki.hyprland.org/Configuring/Keywords/ for more
        env = "XCURSOR_SIZE,24";

        # See https://wiki.hyprland.org/Configuring/Monitors/
        monitor = [
          "eDP-1,preferred,auto,1.5"
          "DP-3,preferred,0x0,1.0"
          "DP-4,preferred,auto-left,1.0"
        ];

        general =
          {
            # See https://wiki.hyprland.org/Configuring/Variables/ for more

            gaps_in = 5;
            gaps_out = 10;
            border_size = 3;
            "col.active_border" = lib.mkForce "${rgba base07 "ee"} ${rgba base0C "ee"} 45deg";
            "col.inactive_border" = lib.mkForce "${rgba base00 "aa"}";

            #cursor_inactive_timeout = 5;

            resize_on_border = true;

            layout = "dwindle";

            # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
            allow_tearing = false;
          };


        # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
        input = {
          kb_layout = "us";
          #kb_variant =
          #kb_model =
          #kb_options =
          #kb_rules =

          follow_mouse = 2;

          touchpad = {
            natural_scroll = "yes";
            scroll_factor = 0.75;
            clickfinger_behavior = true;
            drag_lock = true;
            disable_while_typing = false;
          };

          sensitivity = 0; # -1.0 - 1.0, 0 means no modification.
        };

        dwindle = {
          # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
          # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
          pseudotile = "yes";
          preserve_split = "yes"; # you probably want this
          special_scale_factor = 0.98;
        };

        master = {
          # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
          #new_is_master = true;
        };

        gestures = {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more
          workspace_swipe = "on";
          workspace_swipe_direction_lock = false;
        };

        misc = {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more
          force_default_wallpaper = 0; # Set to 0 to disable the anime mascot wallpapers
          animate_manual_resizes = true;
        };
        decoration = {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more

          rounding = 10;

          blur = {
            enabled = true;
            size = 3;
            passes = 1;
          };

          drop_shadow = "yes";
          shadow_range = 4;
          shadow_render_power = 3;
          dim_special = 0.4;
        };

        animations = {
          enabled = "yes";
          bezier = "myBezier, 0.05, 0.5, 0.1, 1.05";
          animation = [
            "windows, 1, 3, myBezier"
            "windowsOut, 1, 3, default, popin 80%"
            "border, 1, 5, default"
            "borderangle, 1, 4, default"
            "fade, 1, 3, default"
            "workspaces, 1, 3, default"
            "specialWorkspace, 1, 2, default, slidefadevert"
          ];
        };

        xwayland = {
          use_nearest_neighbor = false;
        };

        "$mainMod" = "SUPER";

        bind =
          let
            # Create workspaces, 10 at a time, using number keys
            workspaces = mod: num1: builtins.concatLists (builtins.genList
              (
                x:
                let
                  ws =
                    let
                      c = (x + 1) / 10;
                    in
                    toString (x + 1 - (c * 10));
                in
                [
                  "$mainMod ${mod}, ${ws}, workspace, ${toString (x + num1)}"
                  "$mainMod ${mod} SHIFT, ${ws}, movetoworkspace, ${toString (x + num1)}"
                ]
              )
              10);

            specialWorkspace = key: name: [
              "$mainMod, ${key}, togglespecialworkspace, ${name}"
              "$mainMod SHIFT, ${key}, movetoworkspace, special:${name}"
            ];

            windowManipulation = key1: key2: direction: [
              # Move focus with mainMod + arrow keys
              "$mainMod, ${key1}, movefocus, ${direction}"
              "$mainMod, ${key2}, movefocus, ${direction}"
              # Move windows with mainMod + Shift + Arrow/Vim Keys
              "$mainMod SHIFT, ${key1}, movewindow, ${direction}"
              "$mainMod SHIFT, ${key2}, movewindow, ${direction}"
            ];
          in
          [
            "$mainMod, Return, exec, kitty"
            "$mainMod SHIFT, Return, exec, zen"
            "$mainMod SHIFT, Q, killactive, "
            "$mainMod SHIFT, E, exit, "
            "$mainMod SHIFT, N, exec, nautilus"
            "$mainMod SHIFT, F, togglefloating, "
            "$mainMod, F, fullscreen, "
            "$mainMod CTRL, F, fullscreenstate, "
            "$mainMod, P, pseudo," # dwindle
            "$mainMod, slash, pin,"

            "$mainMod SHIFT, slash, pin,"
            "$mainMod SHIFT, slash, togglefloating,"
            "$mainMod SHIFT, slash, resizeactive, 730 470"
            "$mainMod SHIFT, slash, moveactive, 100%-740 100%-480"

            "$mainMod SHIFT, T, togglegroup,"
            "$mainMod, T, changegroupactive,"
            "$mainMod SHIFT CTRL, T, lockactivegroup,"

            "$mainMod, backspace, togglesplit, # dwindle"
            "$mainMod SHIFT, escape, exec, ${swaylock}"
            "$mainMod, N, exec, ${pkgs.networkmanager_dmenu}/bin/networkmanager_dmenu"

            # WM control
            "$mainMod SHIFT, R, exec, hyprctl reload"

            # Scroll through existing workspaces with mainMod + scroll
            "$mainMod, mouse_down, workspace, e+1"
            "$mainMod, mouse_up, workspace, e-1"

            # Clipboard manager
            "$mainMod, V, exec, cliphist list | wofi --dmenu | cliphist decode | wl-copy"
          ]

          ++ windowManipulation "left" "H" "l"
          ++ windowManipulation "right" "L" "r"
          ++ windowManipulation "up" "K" "u"
          ++ windowManipulation "down" "J" "d"

          ++ workspaces "" 1
          ++ workspaces "CTRL" 11
          ++ workspaces "CTRL ALT" 21

          ++ specialWorkspace "A" "ferdium"
          ++ specialWorkspace "M" "music"
          ++ specialWorkspace "S" "social"
        ;

        # Activates on release of keys
        bindr = [
          # Wofi
          "$mainMod, space, exec, pkill wofi || wofi --show  drun"
          "$mainMod SHIFT, space, exec, pkill wofi || wofi --show run"
        ];

        # Mouse binds
        bindm = [
          # Move/resize windows with mainMod + LMB/RMB and dragging
          "$mainMod, mouse:272, movewindow"
          "$mainMod, mouse:273, resizeactive"
        ];

        # Works on lockscreen
        bindl = [
          # Laptop lid
          ",switch:Lid Switch,exec,${swaylock}"

          # Function key bind
          ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_SINK@ toggle"
          ", XF86AudioPrev, exec, ${playerctl} previous"
          ", XF86AudioPlay, exec, ${playerctl} play-pause"
          ", XF86AudioNext, exec, ${playerctl} next"

          "CTRL, print, exec, ${grimblast} copy area"
          ", print, exec, ${grimblast} copy output"
          "SHIFT CTRL, print, exec, ${screenshot-area-editor}/bin/screenshot-area-editor.sh"
          "SHIFT, print, exec, ${screenshot-fullscreen-editor}/bin/screenshot-fullscreen-editor.sh"
        ];

        # Works on lockscreen and repeats when held
        bindle = [
          # Function key binds
          ", XF86AudioLowerVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_SINK@ 5%-"
          ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_SINK@ 5%+"
          ", XF86MonBrightnessDown, exec, brightnessctl -d 'amdgpu_bl1' set 5%-"
          ", XF86MonBrightnessUp, exec, brightnessctl -d 'amdgpu_bl1' set 5%+"
        ];

        # Example windowrule v1
        #windowrule = [ "float, ^(kitty)$" ];
        # Example windowrule v2
        # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
        # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more

        windowrulev2 = [
          "float,title:^(Picture-in-Picture)$"
          "noinitialfocus,title:^(Picture-in-Picture)$"
          "pin,title:^(Picture-in-Picture)$"
          "size 730 470,title:^(Picture-in-Picture)$"
          "move 100%-740 100%-480,title:^(Picture-in-Picture)$"

          "group set, class:^(Hydrus Client)$"

          "float, class:^(net.sapples.LiveCaptions)$"
          "pin, class:^(net.sapples.LiveCaptions)$"
          "move 127 835, class:^(net.sapples.LiveCaptions)$"

          "float,class:^(pavucontrol)$"
          "size 40% 70%,class:^(pavucontrol)$"
          "center,class:^(pavucontrol)$"

          "float,title:(blueman-manager-wrapped)$"
          "workspace 7,class:^(obsidian)$"
          "workspace special:music,class:^(Plexamp)$"
          "workspace special:social silent,class:^(WebCord)$"
          "workspace special:social silent,class:^(org.telegram.desktop)$"
          "workspace special:ferdium silent,class:^(Ferdium)$"
        ];

        workspace =
          let
            assignWorkspacesToMonitor = num1: monitor:
              builtins.concatLists (builtins.genList
                (
                  x:
                  let
                    ws =
                      let
                        c = (x + 1) / 10;
                      in
                      toString (x + 1 - (c * 10));
                  in
                  [
                    "${toString (x + num1)}, monitor:${monitor}"
                  ]
                )
                10);
          in
          [
            "special:social,gapsin:10,gapsout:10"
            "special:music,on-created-empty: exec ${pkgs.plexamp}/bin/plexamp"
          ] ++ assignWorkspacesToMonitor 11 "DP-3"
          ++ assignWorkspacesToMonitor 1 "eDP-1";

        # See https://wiki.hyprland.org/Configuring/Keywords/ for more
      };
      extraConfig =
        let
          mkSubmap = mod: key: name: ''
            bind = $mainMod ${mod}, ${key}, submap, ${name}
            submap = ${name}'';
          mapReset = "bind = , escape, submap, reset\nsubmap = reset";

          resize-keys = mod: key1: key2: parameters:
            "binde = ${mod}, ${key1}, resizeactive, ${parameters}\nbinde = ${mod}, ${key2}, resizeactive, ${parameters}";
          resize-offset = toString 60;
          resize-big-offset = toString 240;

          qls = key: command: "binde = , ${key}, exec, ${command}\nbinde = , ${key}, submap, reset";
        in
        ''
          # window resize
          ${mkSubmap "" "R" "resize mode"}
          ${resize-keys "" "right" "L" "${resize-offset} 0"}
          ${resize-keys "" "left" "H" "-${resize-offset} 0"}
          ${resize-keys "" "up" "K" "0 -${resize-offset}"}
          ${resize-keys "" "down" "J" "0 ${resize-offset}"}
          ${resize-keys "SHIFT" "right" "L" "${resize-big-offset} 0"}
          ${resize-keys "SHIFT" "left" "H" "-${resize-big-offset} 0"}
          ${resize-keys "SHIFT" "up" "K" "0 -${resize-big-offset}"}
          ${resize-keys "SHIFT" "down" "J" "0 ${resize-big-offset}"}
          ${mapReset}

          # quick launcher
          ${mkSubmap "" "Q" "[c]odium / [f]search / [l]ive captions / [p]lex"}
          ${qls "C" "codium"}
          ${qls "F" "fsearch"}
          ${qls "L" "livecaptions"}
          ${qls "P" "plexmediaplayer"}
          ${mapReset}
        '';
    };
}

