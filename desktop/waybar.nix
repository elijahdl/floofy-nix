{ config, pkgs, lib, lil-cloudy-pkg, ... }:

# with config.lib.stylix.colors;
with config.lib.stylix.colors.withHashtag;
with config.stylix.fonts;

let
  powerprofilecycle = pkgs.writeShellApplication {
    name = "powerprofilecycle.sh";
    text = ''
      # Modified From: https://gitlab.com/lassegs/powerprofilecycle

      # Script to cycle through power-profiles-daemon profiles. Handy for integration
      # with waybar, i3blocks and others. When run it will cycle to next profile and
      # output a corresponding fa-icon for displaying in a bar. With the -m toggle,
      # the script will not cycle profiles, rather just print fa-icon corresponding to
      # current profile.
      #
    
      PSET="powerprofilesctl set"
      PGET="powerprofilesctl get"
      
      POWERSAVER='{ "text": " ", "tooltip": "   Power Saver", "class": "powersaver" }' 
      BALANCED='{ "text": " ", "tooltip": "   Balanced", "class": "balanced" }'
      PERFORMANCE='{ "text": "", "tooltip": " Performance", "class": "performance" }' 
      
      while getopts "mh" opt; do
        case $opt in
          m)
            case $($PGET) in
              performance)
                echo "$PERFORMANCE" && exit 0
                ;;
              power-saver)
                echo "$POWERSAVER" && exit 0
                ;;
              balanced)
                echo "$BALANCED" && exit 0 
            esac
            ;;
          h)
            echo -e "Run script without arguments to cycle power profiles and print icon."
            echo -e "-m Monitor. Get power profile and print icon. \n-h Help. Show this help text"  
            exit 0
            ;;
          *)
            echo "Invalid option. Try -h."
            exit 1
        esac
      done
    
      case $($PGET) in
        performance)
          echo "$POWERSAVER" && $PSET power-saver && exit 0
          ;;
        power-saver)
          echo "$BALANCED" && $PSET balanced && exit 0
          ;;
        balanced)
          echo "$PERFORMANCE" && $PSET performance && exit 0
          ;;
      esac
    
      echo "Could not find power profile match. Is power-profiles-daemon running?"
      exit 1
    '';

  };
  lil-cloudy = lil-cloudy-pkg.packages.x86_64-linux.default;
in
{
  home = {
    packages = [
      lil-cloudy
    ];

    file.".config/lil-cloudy/lil-cloudy-config.toml" = {
      enable = true;
      text = ''
        slides = [
            "{current_weather_icon} {current_temp_f} °F",
            "{current_weather_icon} {current_weather_description}",
            ]
        tooltip = [
            "  Troy:",
            "  {troy:current_weather_icon} {troy:current_weather_description}",
            "   {troy:current_temp_f} °F  󰩏 {troy:current_feels_like_f} °F",
            "  󰖌 {troy:current_humidity} %",
            "   {troy:current_precip_inches} in",
            "  󰈈  {troy:current_visibility_miles} mi",
            "  󰈐  {troy:current_wind_speed_mph} mph {troy:current_wind_direction_16_point}",
            "  󰖜  {troy:next_sunrise} • {troy:next_sunset} 󰖛",
            "  󰽥 {troy:next_moonrise} • {troy:next_moonset} 󰽧",
            "",
            "Albany:",
            "  {alb:current_weather_icon} {alb:current_weather_description}",
            "   {alb:current_temp_f} °F  󰩏 {alb:current_feels_like_f} °F",
            "Schenectady:",
            "  {sch:current_weather_icon} {sch:current_weather_description}",
            "   {sch:current_temp_f} °F  󰩏 {sch:current_feels_like_f} °F",
            "Boston:",
            "  {bos:current_weather_icon} {bos:current_weather_description}",
            "   {bos:current_temp_f} °F  󰩏 {bos:current_feels_like_f} °F",
            ]

        [general]
        cache_timeout_minutes = 20
        program_runs_per_slide = 1
        wttr_endpoint = "wttr.in"
        slide_id = "default"

        [locations]
        alb = "Albany-NY"
        bos = "Boston-MA"
        sch = "Schenectady-NY"
	troy = "Troy-NY"
      '';
    };
  };

  programs = {
    waybar = {
      enable = true;
      settings = {
        mainBar = {
          layer = "top";
          position = "top";
          height = 36;
          output = [
            "eDP-1"
            "DP-4"
            "DP-3"
            "HDMI-A-1"
          ];
          modules-left = [ "sway/workspaces" "sway/mode" "sway/window" "hyprland/workspaces" "hyprland/submap" "hyprland/window" ];
          modules-center = [
            "clock"
          ];
          modules-right = [
            "custom/lil_cloudy"
            "network"
            "bluetooth"
            "pulseaudio"
            "cpu"
            "memory"
            "battery"
            "custom/power_profile"
            "idle_inhibitor"
	    "tray"
          ];
          
          "sway/workspaces" = {
            disable-scroll-wraparound = true;
          };

          "hyprland/submap" = {
            format = "{}";
            tooltip = false;
            max-length = 200;
          };

          "battery" = {
            interval = 10;
            full-at = 80;
            format = "{icon}  {capacity}%";
            format-icons = [ "" "" "" "" "" ];
            states = {
              warning = 30;
              critical = 15;
            };
          };

          "custom/power_profile" = {
            "exec" = "sleep 0.3 && ${powerprofilecycle}/bin/powerprofilecycle.sh -m";
            "interval" = 120;
            "on-click" = "${powerprofilecycle}/bin/powerprofilecycle.sh";
            "exec-on-event" = true;
            "return-type" = "json";
          };

          "custom/lil_cloudy" = {
            "exec" = "${lil-cloudy}/bin/lil-cloudy -j";
            "interval" = 10;
            "on-click" = "xdg-open https://wttr.in";
            "return-type" = "json";
          };

          "idle_inhibitor" = {
            "format" = "{icon}";
            "format-icons" = {
              "activated" = "󰅶";
              "deactivated" = "󰾪";
            };
          };

          "pulseaudio" = {
            "format" = "{icon} {volume}%";
            "format-bluetooth" = " {volume}%";
            "format-muted" = "󰝟";
            "format-icons" = {
              "headphone" = "";
              "hands-free" = "󰋎";
              "headset" = "";
              "phone" = "";
              "portable" = "";
              "car" = "";
              "default" = [ "" "" ];
            };
            "scroll-step" = 1;
            "on-click" = "pavucontrol";
            "ignored-sinks" = [ "Easy Effects Sink" ];
          };

          "bluetooth" = {
            "on-click" = "${pkgs.blueman}/bin/blueman-manager";
            "format" = "";
            "format-disabled" = "󰂲"; # an empty format will hide the module
            "format-connected" = " (󱘖 {num_connections})";
            "tooltip-format" = "{controller_alias}\t{controller_address}";
            "tooltip-format-connected" = "{controller_alias}\t{controller_address}\n\n{device_enumerate}";
            "tooltip-format-enumerate-connected" = "{device_alias}\t{device_address}";
          };

          "cpu" = {
            "interval" = 3;
            "format" = " {usage}%";
            "max-length" = 10;
          };

          "memory" = {
            "interval" = 3;
            "format" = " {}%";
            "max-length" = 10;
          };

          "network" = {
            #"interface" = "wlan0";
            "interval" = 2;
            "format" = "{ifname}";
            "format-wifi" = " {ipaddr}";
            "format-ethernet" = "󰊗 {ipaddr}";
            "format-disconnected" = ""; #An empty format will hide the module.
            "tooltip-format" = "󰊗  {ifname} via {gwaddr}\n {bandwidthUpBits}\n {bandwidthDownBits}";
            "tooltip-format-wifi" = "  {essid} ({signalStrength}%)\n {bandwidthUpBits}\n {bandwidthDownBits}";
            "tooltip-format-ethernet" = "  {ifname}\n {bandwidthUpBits}\n {bandwidthDownBits}";
            "tooltip-format-disconnected" = "Disconnected";
            "max-length" = 50;
            "on-click" = "${pkgs.networkmanager_dmenu}/bin/networkmanager_dmenu";
          };

          "clock" = {
            interval = 1;
            format = "{:%Y-%m-%d • %H:%M:%S}";
            format-alt = "{:%A, %B %d, %Y - %H:%M:%S}";
            tooltip-format = "<tt><small>{calendar}</small></tt>";
            calendar = {
              mode = "month";
              mode-mon-col = 3;
              weeks-pos = "right";
              on-scroll = 1;
              on-click-right = "mode";
              format = {
                months = "<span color='${base0C}'><b>{}</b></span>";
                days = "<span color='${base07}'><b>{}</b></span>";
                weeks = "<span color='${base0B}'><b>W{}</b></span>";
                weekdays = "<span color='${base0B}'><b>{}</b></span>";
                today = "<span color='${base0C}'><b><u>{}</u></b></span>";
              };
            };
            actions = {
              on-click-right = "mode";
              on-click-forward = "tz_up";
              on-click-backward = "tz_down";
              on-scroll-up = "shift_up";
              on-scroll-down = "shift_down";
            };
          };
        };
      };
      style = ''
        @define-color base00 ${base00}; @define-color base01 ${base01}; @define-color base02 ${base02}; @define-color base03 ${base03};
        @define-color base04 ${base04}; @define-color base05 ${base05}; @define-color base06 ${base06}; @define-color base07 ${base07};

        @define-color base08 ${base08}; @define-color base09 ${base09}; @define-color base0A ${base0A}; @define-color base0B ${base0B};
        @define-color base0C ${base0C}; @define-color base0D ${base0D}; @define-color base0E ${base0E}; @define-color base0F ${base0F};

        * {
            font-family: ${sansSerif.name};
            font-size: ${builtins.toString sizes.desktop}pt;
        }

        window#waybar, tooltip, tooltip label {
            background: alpha(@base00, ${with config.stylix.opacity; builtins.toString desktop});
            color: @base07;
            padding: 5px;
        }

      ''
      + (builtins.readFile ./waybar.css);
    };
  };
}


