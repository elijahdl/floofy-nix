{ config, lib, pkgs, ... }:

let
  brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
  grimblast = "${pkgs.grimblast}/bin/grimblast";
  playerctl = "${pkgs.playerctl}/bin/playerctl";
  swaybg = "${pkgs.swaybg}/bin/swaybg";
  swaylock = "/usr/bin/swaylock";
  swaymsg = "swaymsg";
  waybar = "/usr/bin/waybar";
  wezterm = "/sbin/wezterm";

  screenshot-fullscreen-editor = pkgs.writeShellApplication {
    name = "screenshot-fullscreen-editor.sh";
    text = ''
      export GRIMBLAST_EDITOR=ksnip
      ${grimblast} --cursor edit
    '';
  };
  screenshot-area-editor = pkgs.writeShellApplication {
    name = "screenshot-area-editor.sh";
    text = ''
      export GRIMBLAST_EDITOR=ksnip
      ${grimblast} --cursor edit area
    '';
  };
in
{
  home = {
    packages = with pkgs; [
      cliphist
      ksnip
      networkmanager_dmenu
      wl-clipboard
    ];

    file = {
      ".config/networkmanager-dmenu/config.ini".text = ''
        [dmenu]
        dmenu_command = ${pkgs.wofi}/bin/wofi --show dmenu
        # # Note that dmenu_command can contain arguments as well like:
        # # `dmenu_command = rofi -dmenu -i -theme nmdm`
        # # `dmenu_command = rofi -dmenu -width 30 -i`
        # # `dmenu_command = dmenu -i -l 25 -b -nb #909090 -nf #303030`
        # # `dmenu_command = fuzzel --dmenu`
        rofi_highlight = true
        # compact = <True or False> # (Default: False). Remove extra spacing from display
        # pinentry = <Pinentry command>  # (Default: None) e.g. `pinentry-gtk`
        # wifi_chars = <string of 4 unicode characters representing 1-4 bars strength>
        # wifi_chars = ▂▄▆█
        # wifi_icons = <characters representing signal strength as an icon>
        wifi_icons = 󰤯󰤟󰤢󰤥󰤨
        # format = <Python style format string for the access point entries>
        # format = {name}  {sec}  {bars}
        # # Available variables are:
        # #  * {name} - Access point name
        # #  * {sec} - Security type
        # #  * {signal} - Signal strength on a scale of 0-100
        # #  * {bars} - Bar-based display of signal strength (see wifi_chars)
        # #  * {icon} - Icon-based display of signal strength (see wifi_icons)
        # #  * {max_len_name} and {max_len_sec} are the maximum lengths of {name} / {sec}
        # #    respectively and may be useful for formatting.
        # list_saved = <True or False> # (Default: False) list saved connections

        [dmenu_passphrase]
        # # Uses the -password flag for Rofi, -x for bemenu. For dmenu, sets -nb and
        # # -nf to the same color or uses -P if the dmenu password patch is applied
        # # https://tools.suckless.org/dmenu/patches/password/
        # obscure = True
        # obscure_color = #222222

        [pinentry]
        # description = <Pinentry description> (Default: Get network password)
        # prompt = <Pinentry prompt> (Default: Password:)

        [editor]
        terminal = kitty
        # gui_if_available = <True or False> (Default: True)

        [nmdm]
        # rescan_delay = <seconds>  # (seconds to wait after a wifi rescan before redisplaying the results)
      '';
    };
  };

  programs = {
    swaylock = {
      enable = true;
      package = pkgs.swaylock-effects;
      settings = {
        daemonize = true;
        effect-blur = "50x10";
      };
    };
  };

  wayland = {
    windowManager.sway =
      let
        rgb = color: "rgb(${color})";
        rgba = color: alpha: "rgba(${color}${alpha})";
        swaylock_command = "${swaylock} -f -c 111111";
        swayidle_command = "swayidle -w timeout 570 '${swaylock_command}' timeout 600 'swaymsg \"output * power off\"' resume 'swaymsg \"output * power on\"' before-sleep '${swaylock_command}'";
      in
      {
        enable = true;
        checkConfig = true;

        systemd.enable = false;
        extraConfigEarly = ''
          include /etc/sway/config.d/*
    
          bindgesture swipe:right workspace prev
          bindgesture swipe:left workspace next
        '';


        config = {
          bars = lib.mkOverride 10 [ ];

          output = {
            HDMI-A-1 = {
              mode = "2560x1440";
              pos = "0 0";
            };
            DP-3 = {
              mode = "1920x1080";
              pos = "2560 0";
            };
            eDP-1 = {
              scale = "1.50";
            };
          };

          input = {
            "type:touchpad" = {
              tap = "enabled";
              natural_scroll = "enabled";
              dwt = "disabled";
              clickfinger_button_map = "lrm";
              tap_button_map = "lrm";
              click_method = "clickfinger";
            };
          };

          seat = {
            "*" = {
              hide_cursor = "when-typing enable";
              xcursor_theme = lib.mkForce "Adwaita 32";
            };
          };

          startup = [
            { command = "${swayidle_command}"; }

            { command = "dunst"; }

            # { command = "pkill swaybg && swaybg -i home/elijahdl/sync/wallpapers/david-monje-3THn0EN_Ydo-unsplash.jpg -m fill"; always = true; }
            { command = "pkill waybar ; ${waybar}"; always = true; }

            # xdg-desktop-portal
            { command = "systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP"; }
            { command = "exec hash dbus-update-activation-environment 2>/dev/null && dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP XDG_CURRENT_DESKTOP=sway"; }
	    { command = "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"; }

            # Clipboard manager
            { command = "wl-paste --type text --watch cliphist store"; }
            { command = "wl-paste --type image --watch cliphist store"; }

            # Desktop applications
            { command = "zen-browser"; }
            { command = "telegram-desktop"; }
            { command = "vesktop"; }
            { command = "ferdium"; }
            { command = "obsidian"; }
          ];

          assigns = {
            "1" = [{ app_id = "^zen-"; }];
            "17" = [{ class = "^obsidian$"; }];
            "19" = [{ class = "^vesktop$"; } { app_id = "^org.telegram.desktop$"; }];
            "20" = [{ class = "^Ferdium$"; }];
          };

          focus = {
            followMouse = false;
          };

          workspaceAutoBackAndForth = true;

          gaps = {
            inner = 5;
            outer = 5;
          };

          window = {
            border = 4;
            titlebar = false;

            commands = [
              {
                command = "floating enable";
                criteria = {
                  title = "Picture-in-Picture";
                };
              }
              {
                command = "sticky enable";
                criteria = {
                  title = "Picture-in-Picture";
                };
              }
              {
                command = "resize width 480 px height 260 px";
                criteria = {
                  title = "Picture-in-Picture";
                };
              }
              {
                command = "move position 60 ppt 55 ppt";
                criteria = {
                  title = "Picture-in-Picture";
                };
              }
              {
                command = "sticky enable";
                criteria = {
                  app_id = "net.sapples.LiveCaptions";
                };
              }
              {
                command = "move down 300 px";
                criteria = {
                  app_id = "net.sapples.LiveCaptions";
                };
              }
            ];
          };

          modifier = "Mod4";

          keybindings =
            let
              modifier = config.wayland.windowManager.sway.config.modifier;
            in
            lib.mkForce {
              # WM Controls
              "${modifier}+Return" = "exec ${wezterm}";
              "${modifier}+Shift+q" = "kill";
              "${modifier}+Space" = "exec wofi --show drun";
              "${modifier}+Shift+Space" = "exec wofi --show run";
              "${modifier}+Shift+n" = "exec nautilus";

              "${modifier}+Shift+Ctrl+m" = "output HDMI-A-1 toggle";
              "${modifier}+Escape" = "exec ${swaylock_command}";
              "${modifier}+Shift+s" = "exec systemctl suspend";
              "${modifier}+Shift+c" = "reload";
              "${modifier}+Shift+e" = "exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";

              # Layout
              "${modifier}+Backspace" = "splitt";
              "${modifier}+T" = "layout toggle tabbed";

              "${modifier}+f" = "fullscreen toggle";
              "${modifier}+Shift+f" = "floating toggle";

              # Clipboard manager
              "${modifier}+v" = "exec cliphist list | wofi --dmenu | cliphist decode | wl-copy";

              # Network manager
              "${modifier}+n" = "exec ${pkgs.networkmanager_dmenu}/bin/networkmanager_dmenu";

              # Media controls
              "XF86AudioRaiseVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ +5%";
              "XF86AudioLowerVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ -5%";
              "XF86AudioMute" = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";
              "XF86AudioPlay" = "exec playerctl play-pause";
              "XF86AudioNext" = "exec playerctl next";
              "XF86AudioPrev" = "exec playerctl previous";

              # Screenshots
              "Print" = "exec ${grimblast} copy output";
              "Ctrl+Print" = "exec ${grimblast} copy area";
              "Shift+Print" = "exec ${screenshot-fullscreen-editor}/bin/screenshot-fullscreen-editor.sh";
              "Ctrl+Shift+Print" = "exec ${screenshot-area-editor}/bin/screenshot-area-editor.sh";

              "${modifier}+Down" = "focus down";
              "${modifier}+Up" = "focus up";
              "${modifier}+Left" = "focus left";
              "${modifier}+Right" = "focus right";
              "${modifier}+j" = "focus down";
              "${modifier}+k" = "focus up";
              "${modifier}+h" = "focus left";
              "${modifier}+l" = "focus right";

              "${modifier}+Shift+Down" = "move down";
              "${modifier}+Shift+Up" = "move up";
              "${modifier}+Shift+Left" = "move left";
              "${modifier}+Shift+Right" = "move right";
              "${modifier}+Shift+j" = "move down";
              "${modifier}+Shift+k" = "move up";
              "${modifier}+Shift+h" = "move left";
              "${modifier}+Shift+l" = "move right";

              "${modifier}+r" = "mode resize";

              "${modifier}+0" = "workspace number 10";
              "${modifier}+1" = "workspace number 1";
              "${modifier}+2" = "workspace number 2";
              "${modifier}+3" = "workspace number 3";
              "${modifier}+4" = "workspace number 4";
              "${modifier}+5" = "workspace number 5";
              "${modifier}+6" = "workspace number 6";
              "${modifier}+7" = "workspace number 7";
              "${modifier}+8" = "workspace number 8";
              "${modifier}+9" = "workspace number 9";

              "${modifier}+Ctrl+0" = "workspace number 20";
              "${modifier}+Ctrl+1" = "workspace number 11";
              "${modifier}+Ctrl+2" = "workspace number 12";
              "${modifier}+Ctrl+3" = "workspace number 13";
              "${modifier}+Ctrl+4" = "workspace number 14";
              "${modifier}+Ctrl+5" = "workspace number 15";
              "${modifier}+Ctrl+6" = "workspace number 16";
              "${modifier}+Ctrl+7" = "workspace number 17";
              "${modifier}+Ctrl+8" = "workspace number 18";
              "${modifier}+Ctrl+9" = "workspace number 19";

              "${modifier}+Ctrl+Alt+0" = "workspace number 30";
              "${modifier}+Ctrl+Alt+1" = "workspace number 21";
              "${modifier}+Ctrl+Alt+2" = "workspace number 22";
              "${modifier}+Ctrl+Alt+3" = "workspace number 23";
              "${modifier}+Ctrl+Alt+4" = "workspace number 24";
              "${modifier}+Ctrl+Alt+5" = "workspace number 25";
              "${modifier}+Ctrl+Alt+6" = "workspace number 26";
              "${modifier}+Ctrl+Alt+7" = "workspace number 27";
              "${modifier}+Ctrl+Alt+8" = "workspace number 28";
              "${modifier}+Ctrl+Alt+9" = "workspace number 29";

              "${modifier}+Shift+0" = "move container to workspace number 10";
              "${modifier}+Shift+1" = "move container to workspace number 1";
              "${modifier}+Shift+2" = "move container to workspace number 2";
              "${modifier}+Shift+3" = "move container to workspace number 3";
              "${modifier}+Shift+4" = "move container to workspace number 4";
              "${modifier}+Shift+5" = "move container to workspace number 5";
              "${modifier}+Shift+6" = "move container to workspace number 6";
              "${modifier}+Shift+7" = "move container to workspace number 7";
              "${modifier}+Shift+8" = "move container to workspace number 8";
              "${modifier}+Shift+9" = "move container to workspace number 9";

              "${modifier}+Shift+Ctrl+0" = "move container to workspace number 20";
              "${modifier}+Shift+Ctrl+1" = "move container to workspace number 11";
              "${modifier}+Shift+Ctrl+2" = "move container to workspace number 12";
              "${modifier}+Shift+Ctrl+3" = "move container to workspace number 13";
              "${modifier}+Shift+Ctrl+4" = "move container to workspace number 14";
              "${modifier}+Shift+Ctrl+5" = "move container to workspace number 15";
              "${modifier}+Shift+Ctrl+6" = "move container to workspace number 16";
              "${modifier}+Shift+Ctrl+7" = "move container to workspace number 17";
              "${modifier}+Shift+Ctrl+8" = "move container to workspace number 18";
              "${modifier}+Shift+Ctrl+9" = "move container to workspace number 19";

              "${modifier}+Shift+Ctrl+Alt+0" = "move container to workspace number 30";
              "${modifier}+Shift+Ctrl+Alt+1" = "move container to workspace number 21";
              "${modifier}+Shift+Ctrl+Alt+2" = "move container to workspace number 22";
              "${modifier}+Shift+Ctrl+Alt+3" = "move container to workspace number 23";
              "${modifier}+Shift+Ctrl+Alt+4" = "move container to workspace number 24";
              "${modifier}+Shift+Ctrl+Alt+5" = "move container to workspace number 25";
              "${modifier}+Shift+Ctrl+Alt+6" = "move container to workspace number 26";
              "${modifier}+Shift+Ctrl+Alt+7" = "move container to workspace number 27";
              "${modifier}+Shift+Ctrl+Alt+8" = "move container to workspace number 28";
              "${modifier}+Shift+Ctrl+Alt+9" = "move container to workspace number 29";
            };

          modes = {
            resize = {
              Down = "resize grow height 60 px";
              Up = "resize shrink height 60 px";
              Right = "resize grow width 60 px";
              Left = "resize shrink width 60 px";
              j = "resize grow height 60 px";
              k = "resize shrink height 60 px";
              l = "resize grow width 60 px";
              h = "resize shrink width 60 px";

              "Shift+Down" = "resize grow height 240 px";
              "Shift+Up" = "resize shrink height 240 px";
              "Shift+Right" = "resize grow width 240 px";
              "Shift+Left" = "resize shrink width 240 px";
              "Shift+j" = "resize grow height 240 px";
              "Shift+k" = "resize shrink height 240 px";
              "Shift+l" = "resize grow width 240 px";
              "Shift+h" = "resize shrink width 240 px";

              Return = "mode default";
              Escape = "mode default";
            };
          };

          workspaceOutputAssign = [
            { workspace = "10"; output = "HDMI-A-1"; }
            { workspace = "1"; output = "HDMI-A-1"; }
            { workspace = "2"; output = "HDMI-A-1"; }
            { workspace = "3"; output = "HDMI-A-1"; }
            { workspace = "4"; output = "HDMI-A-1"; }
            { workspace = "5"; output = "HDMI-A-1"; }
            { workspace = "6"; output = "HDMI-A-1"; }
            { workspace = "7"; output = "HDMI-A-1"; }
            { workspace = "8"; output = "HDMI-A-1"; }
            { workspace = "9"; output = "HDMI-A-1"; }

            { workspace = "20"; output = "DP-3"; }
            { workspace = "11"; output = "DP-3"; }
            { workspace = "12"; output = "DP-3"; }
            { workspace = "13"; output = "DP-3"; }
            { workspace = "14"; output = "DP-3"; }
            { workspace = "15"; output = "DP-3"; }
            { workspace = "16"; output = "DP-3"; }
            { workspace = "17"; output = "DP-3"; }
            { workspace = "18"; output = "DP-3"; }
            { workspace = "19"; output = "DP-3"; }
          ];
        };
      };
  };
}
