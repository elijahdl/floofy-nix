{ config, pkgs, lib, ... }: {
  home = {
    shellAliases = { };
  };

  programs = {
    zsh = {
      initExtraFirst = "";
      # initExtra = "";
      enable = true;
      # zprof.enable = true;
      autocd = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      enableVteIntegration = true;
      history = {
        extended = true;
        ignoreSpace = true;
        save = 10000;
        share = true;
        size = 10000;

      };
      oh-my-zsh = {
        enable = true;
        plugins = [
          "autojump"
          "common-aliases"
          "dirhistory"
          "git"
          "rust"
        ];
        theme = "ys";
      };
      plugins = [
        {
          name = "zsh-nix-shell";
          file = "nix-shell.plugin.zsh";
          src = pkgs.fetchFromGitHub {
            owner = "chisui";
            repo = "zsh-nix-shell";
            rev = "v0.7.0";
            sha256 = "149zh2rm59blr2q458a5irkfh82y3dwdich60s9670kl3cl5h2m1";
          };
        }
      ];
      initExtra = ''
	export PATH="/home/elijahdl/.cargo/bin:/home/elijahdl/.local/bin:$PATH"
      '';
    };
  };
}
