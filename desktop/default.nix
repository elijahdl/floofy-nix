{ ... }:

{
  imports = [
    ./eww.nix
    ./hyprland.nix
    ./waybar.nix
  ];
}
