{
  description = "FLOOF's NixOs Flake";

  nixConfig = {
    experimental-features = [ "nix-command" "flakes" ];
    substituters = [
      "https://cache.nixos.org/"
    ];
    extra-substituters = [
      "https://nix-community.cachix.org"
      "https://hyprland.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.90.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # home-manager, used for managing user configuration
    home-manager = {
      url = "github:nix-community/home-manager";
      # The `follows` keyword in inputs is used for inheritance.
      # Here, `inputs.nixpkgs` of home-manager is kept consistent with
      # the `inputs.nixpkgs` of the current flake,
      # to avoid problems caused by different versions of nixpkgs.
      inputs.nixpkgs.follows = "nixpkgs";
    };

    stylix.url = "github:danth/stylix";

    getchoo.url = "github:getchoo/nix-exprs";

    zen-browser.url = "github:MarceColl/zen-browser-flake";

    lil-cloudy-pkg.url = "gitlab:elijahdl/lil-cloudy";
  };

  outputs =
    { self
    , nixpkgs
    , lix-module
    , home-manager
    , nixos-hardware
    , stylix
    , getchoo
    , lil-cloudy-pkg
    , zen-browser
    , ...
    }@inputs: {
      defaultPackage.x86_64-linux = home-manager.defaultPackage.x86_64-linux;
      homeConfigurations = {
        "elijahdl" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs { system = "x86_64-linux"; };

          extraSpecialArgs = { inherit stylix lil-cloudy-pkg; };

          modules = [ 
            # ./home.nix
            stylix.homeManagerModules.stylix
            ./home-arch-linux.nix
            ];
        };
      };
      nixosConfigurations = {
        "legoshi" = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";

          specialArgs = inputs;

          modules = [
            ./configuration.nix
            ./legoshi-hardware-configuration.nix
            ./desktop/nixos.nix

	    #lix-module.nixosModules.default

            stylix.nixosModules.stylix

            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;

              home-manager.users.elijahdl = import ./home.nix;

              # Optionally, use home-manager.extraSpecialArgs to pass arguments to home.nix

              home-manager.extraSpecialArgs = { inherit stylix lil-cloudy-pkg; };
            }
          ];
        };
      };
    };
}
