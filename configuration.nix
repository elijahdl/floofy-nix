# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, getchoo, zen-browser, ... }:

{
  imports = [ ];

  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    trusted-users = [ "elijahdl" ];
    auto-optimise-store = true;
    system-features = [ "kvm" "big-parallel" ];
    max-jobs = 16;
  };

  # nix.channel.enable = false;

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 1w";
  };

  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [
      "electron-24.8.6"
      "electron-25.9.0"
    ];
  };

  boot = {
    extraModprobeConfig = ''
      options snd-hda-intel model=dell-deadset-multi
    '';
    loader.systemd-boot = {
      enable = true;
      configurationLimit = 25;
      memtest86.enable = true;
    };
    loader.efi.canTouchEfiVariables = true;
    kernelModules = [ "amd-pstate=passive" ];
    kernelPackages = pkgs.linuxPackages_6_11;
    kernelParams = [ "splash" "initcall_blacklist=acpi_cpufreq_init" "modprobe.blacklist=dvb_usb_rtl28xxu" ];
    kernel = {
      sysctl = {
        "vm.swappiness" = 90; # when swapping to ssd, otherwise change to 1
        "vm.vfs_cache_pressure" = 50;
        "vm.dirty_background_ratio" = 20;
        "vm.dirty_ratio" = 50;
        # these are the zen-kernel tweaks to CFS defaults (mostly)
        "kernel.sched_latency_ns" = 4000000;
        # should be one-eighth of sched_latency (this ratio is not
        # configurable, apparently -- so while zen changes that to
        # one-tenth, we cannot):
        "kernel.sched_min_granularity_ns" = 500000;
        "kernel.sched_wakeup_granularity_ns" = 50000;
        "kernel.sched_migration_cost_ns" = 250000;
        "kernel.sched_cfs_bandwidth_slice_us" = 3000;
        "kernel.sched_nr_migrate" = 128;
      };
    };
    plymouth = {
      enable = false;
    };
  };

  security.polkit = {
    enable = true;
    extraConfig = ''
      polkit.addRule(function(action, subject) {
        if (
          subject.isInGroup("users")
            && (
              action.id == "org.freedesktop.login1.reboot" ||
              action.id == "org.freedesktop.login1.reboot-multiple-sessions" ||
              action.id == "org.freedesktop.login1.power-off" ||
              action.id == "org.freedesktop.login1.power-off-multiple-sessions"
            )
          )
        {
          return polkit.Result.YES;
        }
      })
    '';
  };

  systemd.user = {
    services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };

    services.update-nix-index-database = {
      serviceConfig = {
        Type = "oneshot";
        Nice = 30;
      };
      path = with pkgs; [ nix-index ];
      script = ''
        nix-index
      '';
    };
    timers.update-nix-index-database = {
      wantedBy = [ "timers.target" ];
      partOf = [ "update-nix-index-database.service" ];
      timerConfig = {
        OnCalendar = "weekly";
        Unit = "update-nix-index-database.service";
      };
    };
  };

  networking = {
    hostName = "legoshi";
    networkmanager.enable = true;
    firewall = {
      enable = true;
      trustedInterfaces = [ "tailscale0" ];
      # allowedTCPPorts = [ ... ];
      # allowedUDPPorts = [ ... ];
    };
  };

  time.timeZone = "America/New_York";
  i18n.defaultLocale = "en_US.UTF-8";
  console = lib.mkDefault {
    font = "Lat2-Terminus16";
    keyMap = "us";
    useXkbConfig = true; # use xkb.options in tty.
  };

  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = true;
      input = {
        General = {
          ClassicBondedOnly = true;
        };
      };
    };
    keyboard.zsa.enable = true;
    logitech.wireless.enable = true;
    rtl-sdr.enable = true;
    sensor.iio.enable = true;
    graphics = {
      ## radv: an open-source Vulkan driver from freedesktop
      #driSupport = true;
      #driSupport32Bit = true;

      ## amdvlk: an open-source Vulkan driver from AMD
      extraPackages = [ pkgs.amdvlk ];
      extraPackages32 = [ pkgs.driversi686Linux.amdvlk ];
    };

  };

  users.extraGroups.vboxusers.members = [ "elijahdl" ];
  users.users.elijahdl = {
    isNormalUser = true;
    uid = 11011;
    extraGroups = [
      "audio"
      "docker"
      "input"
      "libvirt"
      "libvirtd"
      "libvirt-qemu"
      "pipewire"
      "plugdev"
      "tty"
      "video"
      "wheel"
      "wireshark"
    ];
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/Gyk2wClfhVtrI8eFv5uq1ZeS8D+8Z2gL0X8OZrasiDudQeuFypTH33tb7TllAwyz+yAU5BK7e/r7lIpgLC7/bYAhE59TyoQotMWmMBXGuAQmPPTQfU8JDB4CjCvS+EfYp5BlfNJ9IJPnGiow++lBGQ8WvvXSzINMilWzJU/x7VW16gZs263iG17C7XNEB04ksE807C97OPPi6W2OxjFtbRU+KIAYo2hLqY2LdwRxikeCsTetqAQOiLQop4bQCd5ikei/o7mg6OLyUaDSlpVhg9EBfYFXe+r/tgYU+UcDXkO6HKq87nhNb1PkF4SHIqeKnVKN9yv/Ke8ap320DX+f/ok3QCq2T/V2VZd9JabrcVG24mglvTqZ8guZnFdg6zQTiMjF+0nztov4ibATZU0MpfSJk3G3P6O2Yn3Mh8ysqDguM/0Ehy6N0ETzu4aPjNBOBlQFdG7DdNC7BOHkeqdVqBntlejZdCf0u2VUoHiQtq7Z6v4FakKZJzvDN3xmUyE= elijahdl@journey"
    ];
    hashedPassword = "$6$gdUPSRFP8Jrykmd.$FsOhN5nHoPnbqlHGwf3Qxz.3lMk1agN5HgP60wOSN8H4GAoEdnZCztSdZuMRoDbo8qYmotmz44Ma.cfUuqkMW/";
  };

  environment = {
    shellAliases = {
      wifi = "nmcli dev wifi show-password";

      mvc = "mullvad connect";
      mvs = "mullvad status";
      mvd = "mullvad disconnect";

      tsu = "sudo tailscale up --ssh --accept-routes --operator elijahdl --reset";
      tsue = "sudo tailscale up --ssh --accept-routes --exit-node-allow-lan-access --exit-node tails --operator elijahdl --reset";
      tss = "tailscale status";
      tsd = "tailscale down";

      h = "history";
    };
    # List packages installed in system profile. To search, run:
    # $ nix search wget
    systemPackages = with pkgs; with getchoo.packages.${pkgs.system}; [
      zen-browser.packages."${system}".specific
      amdgpu_top
      archivemount
      aria2 # A lightweight multi-protocol & multi-source command-line download utility
      brightnessctl
      btop # replacement of htop/nmon
      comma
      curl
      d-spy
      dnsutils # `dig` + `nslookup`
      ethtool
      eva # A modern replacement for ‘ls’
      file
      firefox
      floorp
      fzf # A command-line fuzzy finder
      gawk
      git
      gnupg
      gnused
      gnutar
      gparted
      gping
      gptfdisk
      gqrx
      htop
      iftop # network monitoring
      iotop # io monitoring
      iperf3
      kitty
      killall
      ldns # replacement of `dig`, it provide the command `drill`
      lf
      libusb1
      lm_sensors # for `sensors` command
      lsof # list open files
      ltrace # library call monitoring
      mtr # A network diagnostic tool
      ncdu
      neovim
      nix-output-monitor
      nmap # A utility for network discovery and security auditing
      p7zip
      pavucontrol
      pciutils # lspci
      ripgrep # recursively searches directories for a regex pattern
      rtl-sdr
      socat # replacement of openbsd-netcat
      solaar
      strace # system call monitoring
      sysstat
      tcpdump
      trash-cli
      tree
      treefetch
      modrinth-app
      trippy
      unzip
      usbutils # lsusb
      watchexec
      wget
      which
      wofi
      xz
      zip
      zstd
    ];
  };

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    git.config = {
      init = {
        defaultBranch = "main";
      };
      safe.directory = [ "/etc/nixos" ];
    };
    mosh.enable = true;
    virt-manager.enable = true;
    wireshark.enable = true;
    zsh = {
      enable = true;
      ohMyZsh = {
        enable = true;
        theme = "ys";
      };
    };
  };

  services = {
    blueman.enable = true;
    fprintd.enable = true;
    fwupd.enable = true;
    libinput.enable = true;
    logind.extraConfig = ''
      # don’t shutdown when power button is short-pressed
      HandlePowerKey=ignore
    '';
    mullvad-vpn.enable = true;
    openssh = {
      enable = true;
    };
    power-profiles-daemon.enable = true;
    printing.enable = true;
    udev = {
      packages = [ pkgs.rtl-sdr ];
      extraRules = ''
        # Ethernet expansion card support
        ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0bda", ATTR{idProduct}=="8156", ATTR{power/autosuspend}="20"
      '';
    };
    syncthing = {
      enable = true;
      user = "elijahdl";
      dataDir = "/home/elijahdl/sync"; # Default folder for new synced folders
      configDir = "/home/elijahdl/.config/syncthing"; # Folder for Syncthing's settings and keys
    };
    system76-scheduler.enable = true;
    tailscale.enable = true;
    xserver = {
      enable = true;
      displayManager.gdm.enable = true;
      #displayManager.autoLogin.user = "elijahdl";
      desktopManager.gnome.enable = true;
    };
  };

  virtualisation = {
    libvirtd = {
      enable = true;
    };
    #virtualbox = {
    #  host.enable = true;
    #  host.enableExtensionPack = true;
    #};
  };

  fonts.packages = with pkgs; [
    liberation_ttf
    mplus-outline-fonts.githubRelease
    dina-font
    nerd-fonts.symbols-only
    # (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
  ];

  stylix = {
    enable = true;
    image = ./style/wallpaper.png;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/darkviolet.yaml";
    targets = {
      plymouth.enable = false;
    };
    cursor = {
      package = pkgs.apple-cursor;
      name = "macOS-Monterey";
      size = 32;
    };
    fonts = {
      serif = {
        package = pkgs.nerd-fonts.ubuntu;
        name = "Ubuntu Nerd Font";
      };

      sansSerif = {
        package = pkgs.nerd-fonts.ubuntu;
        name = "Ubuntu Nerd Font";
      };

      monospace = {
        package = pkgs.nerd-fonts.ubuntu-mono;
        name = "UbuntuMono Nerd Font";
      };

      emoji = {
        package = pkgs.nerd-fonts.symbols-only;
        name = "Symbols Nerd Font";
      };
    };
  };

  qt.platformTheme = "gnome";

  system.stateVersion = "unstable"; # Did you read the comment?
}

