{ config, pkgs, lib, ... }:
{
  home = {
    packages = with pkgs; [
      bcc
      beekeeper-studio
      bpftrace
      cargo-expand
      codecrafters-cli
      direnv
      eclipses.eclipse-java
      erlang
      evcxr
      exercism
      gcc
      git-crypt
      gittyup
      gleam
      hugo
      hyperfine
      jdk
      jetbrains.idea-community-bin
      jq
      lapce
      lazygit
      #manim
      msr-tools
      mysql-client
      nixpkgs-fmt
      nodejs
      python3
      opentofu
      processing
      racket
      rustup
      sbt-with-scala-native
      terraform
      terraformer
      terragrunt
      tiptop
      trace-cmd
      valgrind
      virt-viewer
      vscodium
      yq
    ];

    shellAliases = {
      nd = "nix develop -c";

      nfc = "nix flake check";
      nfs = "nix flake show";
      nfu = "nix flake update";

      ns = "nix-shell -p";

      Gnfu = "git commit -am \"flake update\"";

      Ga = "git add";
      Gc = "git commit";
      GC = "git checkout";
      Gd = "git diff";
      Gfp = "git commit - -amend - -no-edit && git push - -force-with-lease";
      Gi = "git init";
      Gpl = "git pull";
      Gps = "git push";
      Gr = "git restore";
      Gs = "git status";
    };
  };

  programs = {

    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
      coc = {
        enable = true;
      };
      plugins = with pkgs.vimPlugins; [
        coc-rust-analyzer
      ];
      extraConfig = ''
        				set number relativenumber
        				'';
    };

    vscode = {
      enable = false;
      package = pkgs.vscodium;
      extensions = with pkgs.vscode-extensions; [
        mkhl.direnv
        asvetliakov.vscode-neovim
        arrterian.nix-env-selector
        jnoortheen.nix-ide
      ];
      userSettings = lib.mkForce
        {
          "files.autoSave" = "off";
          "workbench.colorTheme" = "Synthax";
          "git.autofetch" = true;
          "window.menuBarVisibility" = "toggle";
          "extensions.experimental.affinity" = {
            "asvetliakov.vscode-neovim" = 1;
          };
          "[nix]" = {
            "enableLanguageServer" = true;
            "serverPath" = "${pkgs.nil}/bin/nil";
          };
          "editor" = {
            "fontFamily" = "'UbuntuMono Nerd Font'";
            "formatOnSave" = true;
          };
          "terminal" = {
            "integrated" = {
              "fontFamily" = "'UbuntuMono Nerd Font'";
            };
          };
        };
    };
  };
}
