{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    audacity
    calibre
    ffmpeg
    filebot
    foliate
    hydrus
    kid3
    losslesscut-bin
    mkvtoolnix
    mpv
    obs-studio
    picard
    plex-media-player
    plexamp
    squeezelite-pulse
    vlc
    webcamoid
    yt-dlp
  ];
}
