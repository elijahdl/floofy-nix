{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    libreoffice
    masterpdfeditor
    pdftk
  ];
}
