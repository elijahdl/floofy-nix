{ config, pkgs, lib, ... }:
{
  home.file = {
    ".config/zellij/config.kdl" = {
      enable = true;
      source = ./zellig-config.kdl;
    };

    ".config/zellij/layouts/default.kdl" = {
      enable = true;
      source = ./default.kdl;
    };

    ".config/zellij/layouts/default.swap.kdl" = {
      enable = true;
      source = ./default.swap.kdl;
    };
  };

  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
  };

  programs.zsh.initExtra = ''
    function zr () { zellij run --name "$*" -- zsh -ic "$*";}
    function zrf () { zellij run --name "$*" --floating -- zsh -ic "$*";}
    function ze () { zellij edit "$*";}
    function zef () { zellij edit --floating "$*";}
  '';

}
