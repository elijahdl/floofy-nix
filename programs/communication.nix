{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    discord
    ferdium
    gnuradio
    gqrx
    newsflash
    sdrangel
    sdrpp
    signal-desktop
    telegram-desktop
    tuba
    webcord
  ];
}
