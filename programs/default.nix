{ ... }:

{
  imports = [
    ./communication.nix
    ./development
    ./making.nix
    ./media.nix
    ./productivity.nix
    ./zellij
  ];
}
