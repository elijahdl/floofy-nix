local wezterm = require 'wezterm'
local act = wezterm.action

wezterm.on('update-right-status', function(window, pane)
    local name = window:active_key_table()
    if name then name = 'TABLE: ' .. name end
    window.set_right_status(name or '')
end)

return {
    audible_bell = "Disabled",
    color_scheme = 'Dark Violet (base16)',
    colors = {visual_bell = '#002222'},
    enable_wayland = true,
    -- font = wezterm.font 'Noto Sans Mono Medium',
    front_end = "WebGpu",
    hide_tab_bar_if_only_one_tab = true,
    keys = {
        {key = 'l', mods = 'ALT', action = wezterm.action.ShowLauncher}, {
            key = '|',
            mods = 'LEADER|SHIFT',
            action = wezterm.action.SplitHorizontal {
                domain = 'CurrentPaneDomain'
            }
        }, {
            key = '_',
            mods = 'LEADER|SHIFT',
            action = wezterm.action.SplitVertical {domain = 'CurrentPaneDomain'}
        },
        {
            key = 'a',
            mods = 'LEADER|CTRL',
            action = wezterm.action.SendString '\x01'
        }
    },
    launch_menu = {{args = {'htop'}, label = 'Launch htop'}},
    leader = {key = 'a', mods = 'CTRL', timeout_milliseconds = 3000},
    use_fancy_tab_bar = false,
    visual_bell = {
        fade_in_function = 'EaseIn',
        fade_in_duration_ms = 50,
        fade_out_function = 'EaseOut',
        fade_out_duration_ms = 50
    },
    window_background_opacity = 0.9
    -- window_frame = {
    --     font = wezterm.font {family = 'Noto Sans Mono', weight = 'Medium'},
    --     active_titlebar_bg = '#333333',
    --     inactive_titlebar_bg = '#000000'
    -- }
}
