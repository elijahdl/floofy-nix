#!/bin/sh

sed -i 's/nvme0n1p1/vda1/g' legoshi_setup.sh
sed -i 's/nvme0n1p2/vda2/g' legoshi_setup.sh
sed -i 's/nvme0n1/vda/g' legoshi_setup.sh
