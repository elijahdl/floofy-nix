{ config, pkgs, lib, ... }:

with config.lib.stylix.colors;

{

  imports = [
    ./programs
    ./desktop
    ./desktop/shell.nix
  ];
  home.username = "elijahdl";
  home.homeDirectory = "/home/elijahdl";

  # link the configuration file in current directory to the specified location in home directory
  # home.file.".config/i3/wallpaper.jpg".source = ./wallpaper.jpg;

  # link all files in `./scripts` to `~/.config/i3/scripts`
  # home.file.".config/i3/scripts" = {
  #   source = ./scripts;
  #   recursive = true;   # link recursively
  #   executable = true;  # make all files executable
  # };

  # encode the file content in nix configuration file directly
  # home.file.".xxx".text = ''
  #     xxx "tray"
  # '';

  stylix = {
    enable = true;
    targets.zellij.enable = false;
    opacity = { applications = 0.9; desktop = 0.9; popups = 0.9; terminal = 0.9; };

  };


  home = {
    packages = with pkgs; [
      anki
      #apostrophe
      autojump
      bitwarden
      cowsay
      freetube
      fsearch
      gimp
      glow # markdown previewer in terminal
      gnome-pomodoro
      gns3-gui
      graphviz
      hugo # static site generator
      inkscape
      ipcalc # it is a calculator for the IPv4/v6 addresses
      keymapp
      livecaptions
      magic-wormhole-rs
      marksman
      marktext
      mastodon-archive
      neofetch
      obsidian
      okular
      pandoc
      pipx
      qalculate-gtk
      rbw
      remmina
      scrcpy
      thunderbird
      transmission_4-gtk
      transmission-remote-gtk
      tut
      ungoogled-chromium
      warp
      weylus
      wireshark
      wol
    ];
    shellAliases = {
      nrs = "sudo nixos-rebuild switch --flake ~/floofy-nix; rehash";
      nrb = "sudo nixos-rebuild boot --flake ~/floofy-nix";

      # Make ssh not be a pain in the neck with kitty
      ssh = "kitten ssh";

      yt-dl = "yt-dlp --restrict-filename -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'";
    };
  };

  programs = {

    bat = {
      enable = true;
    };

    git = {
      enable = true;
      userName = "Elijah Love";
      userEmail = "me@elijahlove.xyz";
      extraConfig = {
        safe.directory = [ "/etc/nixos" ];
        init = {
          defaultBranch = "main";
        };

      };
    };

    kitty.enable = true;

    wezterm = {
      enable = false;
      # extraConfig = ''
      #   				${lib.fileContents ./programs/wezterm/wezterm.lua}
      #   				'';
    };
  };


  services = {
    mpris-proxy.enable = true;
    udiskie.enable = true;
  };

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };


  # This value determines the home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update home Manager without changing this value. See
  # the home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "25.05";

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
